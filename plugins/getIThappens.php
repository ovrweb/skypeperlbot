<?php

require_once "lib/nokogiri_parser.php";
function getIThappens () {
		$i = 0;
		$max = 15000;
		$ua = 'Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19';
		do{
			$i++;
			#echo "-- $i\n";
			$num = rand ( 0 , $max );
			$url = "http://ithappens.me/story/".$num;
			$handle = curl_init($url);
			curl_setopt($handle, CURLOPT_USERAGENT, $ua);
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($handle, CURLOPT_TIMEOUT, 1);
			$response = curl_exec($handle);
			$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

			//echo "-----------{$httpCode}-----------------------\n$response\n";

			if($httpCode == 200){
				curl_close($handle);
				
				$saw = new nokogiri($response);
				$title = $saw->get('.story h1')->toArray();

        		$title = trim($title[0]['#text'][0]);
        		$title = "$url ($title)\n";
				$regexp = '/<div class=\'text\'>(.+?)<\/div>/s';
				

			 	if(preg_match($regexp, $response, $matches)) {
					//print_r($matches);
			  	}else{
					continue;
				}
			
				$matches = trim($matches[1]);
				$matches = str_replace(array("<br>", "<br/>", "<br />", "</p>"), "\n", $matches);
				$matches = str_replace("<p>",	" ",  $matches);
				$matches = str_replace("<li>",	" >",  $matches);
				$matches = strip_tags($matches);
		
				if(strlen($matches) > 700){
					$httpCode = -1; 
					continue;
				}
				return trim($title.$matches);
				
			}else{
				curl_close($handle);
				#echo	$httpCode;		
			}
			//usleep(1000000);
		}while($httpCode != 200 && $i < 40);
		die ("Превышен лимит запросов.");
	}

echo getIThappens ();
