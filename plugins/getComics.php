<?php

require_once "lib/nokogiri_parser.php";
function getComics () {
		$i = 0;
		$time = time();
		do{
			$i++;
			$handle = curl_init("http://comicsia.ru/random/");
			curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($handle,  CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($handle,  CURLOPT_TIMEOUT,        1);
			$response = curl_exec($handle);
			$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
			//echo $i." ".$num." ".$httpCode ."\n";
			if($httpCode == 200){
				global $parser;
				$parser = new nokogiri($response);	
				curl_close($handle);
				$tmpArray = $parser->get ( 'img' )->toArray ();
				$link = $tmpArray[0]['src'] ." ". (!empty($tmpArray[0]['alt']) ? "({$tmpArray[0]['alt']}) " : "");
				//$link = str_replace(" ", "%20", $link);
				//$link = rawurlencode ($link);
				return($link);
			}
		}while(($httpCode != 200 && $i < 3) );
		return "превышен лимит запросов";
	}
echo getComics();
